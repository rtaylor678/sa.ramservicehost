﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace WcfService1
{

    public class DistributorValidator : System.IdentityModel.Selectors.UserNamePasswordValidator
    {

        public override void Validate(string userName, string password)
        {
            throw new Exception("sdfs");

            //if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))

            //    throw new SecurityTokenException("Username and password required");

            //var repository = new DistributorRepository();

            //if (!repository.IsKnownDistributor(userName, password))

            //    throw new FaultException(string.Format("Wrong username ({0}) or password ", userName));

            System.IO.File.WriteAllText(@"C:\Users\JBF\Documents\WriteText.txt", userName + password);
        }

    }


    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service1 : IService1
    {
        Crypt cryptor = new Crypt();
        //private byte[] Key = { 248, 215, 80, 175, 235, 183, 19, 200, 195, 97, 212, 123, 110, 0, 192, 83, 148, 33, 49, 121, 147, 240, 138, 188, 113, 177, 209, 112, 145, 219, 53, 78 };
        //private byte[] Vector = { 86, 102, 197, 82, 75, 101, 134, 69, 250, 94, 79, 187, 146, 83, 18, 128 };
        private byte[] Key = Encoding.UTF8.GetBytes(System.Configuration.ConfigurationManager.AppSettings["Key"].ToString());
        private byte[] Vector = Encoding.UTF8.GetBytes(System.Configuration.ConfigurationManager.AppSettings["Vector"].ToString());

        #region Pull Data
        public DataSet PullData(double dsid)
        {
            System.Data.DataSet ds = new System.Data.DataSet();
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("PropertyName");
            dt.Rows.Add("EquipCntAgitatorsInVessels");
            ds.Tables.Add(dt);
            return ds;
        }
        #endregion

        #region Add Site
        public int addSite(int companyDatasetID, string siteName, string siteLocation, string currency, float exchangeRate, int userNo, string userID)
        {
            using (var db = new WcfService1.RAMDevEntities())
            {
                db.Connection.Open();
                return Convert.ToInt16(db.AddSite(db.Dataset_LU.Single(c => c.DatasetID == companyDatasetID).FacilitySID, companyDatasetID, siteName, null, null, siteLocation, currency, exchangeRate, userNo, userID, null).Single());
            }
        }
        #endregion

        #region Add Unit
        public int addUnit(int siteID, string unitName, string processFamily, string genProductName, string primaryProductName, int userNo, string userID)
        {
            using (var db = new WcfService1.RAMDevEntities())
            {
                db.Connection.Open();
                return Convert.ToInt16(db.AddUnit(siteID, unitName, null, null, processFamily, null, genProductName, primaryProductName, userNo, userID).FirstOrDefault());
            }
        }
        #endregion

        public string RAMEncrypt(string aString, string salt)
        {
            string EncryptionIV = "SACTW0S1G2L3C4D5B6R7M8F9";
            string PartialKey = "SOALCDMSCCHOOL1996DUSTYRHODES";

            // Note that the key and IV must be the same for the encrypt and decrypt calls.
            string results;
            TripleDESCryptoServiceProvider tdesEngine = new TripleDESCryptoServiceProvider();
            string key = salt + PartialKey;
            byte[] keyBytes = Encoding.ASCII.GetBytes(key.Substring(0, 24));
            byte[] ivBytes = Encoding.ASCII.GetBytes(EncryptionIV);

            ICryptoTransform transform = tdesEngine.CreateEncryptor(keyBytes, ivBytes);
            byte[] mesg = Encoding.ASCII.GetBytes(aString);
            byte[] ecn = transform.TransformFinalBlock(mesg, 0, mesg.Length);

            results = Convert.ToBase64String(ecn);

            return (results);
        }


        #region Verify Credentials
        private int VerifyCredentials(string username, string password)
        {
            int LoginID = 0;
            //using (var db = new WcfService1.ExcelEntities())
            using (var db = new WcfService1.RAMDevEntities())
            {
                db.Connection.Open();

                //Login login = db.GetUser(username).SingleOrDefault();
                LogIn login = db.GetUser(username).SingleOrDefault();

                if (login != null)
                {

                    string salt = "dog" + login.ScreenName + "butt" + login.CompanyID + "steelers";
                    string encPwd = RAMEncrypt(password, salt);
                    bool isMatch = encPwd.Equals(login.Password);



                    //### ! TEMPORARY FOR TESTING ! ###
                    if (username == "brian.franze@solomononline.com")
                    {
                        //return login.LoginID;
                        return login.UserNo;
                    }

                    if (CheckPassword(password, login.Password, login.Salt))
                        //LoginID = login.LoginID;
                        LoginID = login.UserNo;
                    else
                        LoginID = 0;
                }
            }
            return LoginID;
        }
        #endregion

        private bool CheckPassword(string password, string dbPassword, byte[] salt)
        {
            //create hash using user supplied password
            string Hashed = string.Empty;
            using (SHA512CryptoServiceProvider sha512 = new SHA512CryptoServiceProvider())
            {
                //byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(deriveBytes + password + password));
                byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(salt + password + password));
                Hashed = Convert.ToBase64String(bb);
            }
            //decrypt password and compare to the user supplied password
            try
            {
                string decryptedString = DecryptPassword(dbPassword, Hashed);
            }
            catch (Exception ex)
            {
                if (ex.Message == "The data is invalid.\r\n")
                    return false;
            }
            return true;
        }

        public string DecryptPassword(string stringToDecrypt, string Hash)
        {
            byte[] decryptedData = ProtectedData.Unprotect(Convert.FromBase64String(stringToDecrypt), Encoding.Unicode.GetBytes(Hash), DataProtectionScope.LocalMachine);
            return Encoding.Unicode.GetString(decryptedData);
        }

        #region Generate Salt
        private byte[] GenerateSalt()
        {
            using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
            {
                byte[] data = new byte[512];
                rngCsp.GetBytes(data);
                return data;
                //string value = Convert.ToBase64String(data);
                //int value2 = BitConverter.ToInt32(data, 0);
                //string value3 = BitConverter.ToString(data);
                //return value;
            }
        }
        #endregion

        #region InitiateHandshake
        public Handshake InitiateHandshake()
        {
            byte[] Salt = GenerateSalt();
            int? SID = 0;
            //using (var db = new WcfService1.ExcelEntities())
            using (var db = new WcfService1.RAMDevEntities())
            {
                try
                {
                    db.Connection.Open();
                    SID = Convert.ToInt32(db.InsertSalt(Salt, DateTime.Now.AddSeconds(30)).SingleOrDefault());
                }
                catch (Exception ex)
                {
                }
            }
            Handshake handshake = new Handshake();
            handshake.Value2 = Salt;
            handshake.sid = SID;
            handshake.Value3 = GenerateEncryptionKey();
            handshake.Value4 = GenerateEncryptionVector();
            return handshake;
        }
        #endregion

        #region Generate Encryption Key
        static public byte[] GenerateEncryptionKey()
        {
            RijndaelManaged rm = new RijndaelManaged();
            rm.GenerateKey();
            return rm.Key;
        }
        #endregion

        #region Generate Encryption Vector
        static public byte[] GenerateEncryptionVector()
        {
            RijndaelManaged rm = new RijndaelManaged();
            rm.GenerateIV();
            return rm.IV;
        }
        #endregion


        //#region Decrypt String
        //public string DecryptString(string EncryptedString, int salt)
        //{
        //    return (EncryptedString != "") ? Decrypt(Encoding.UTF8.GetBytes(EncryptedString), salt) : "";
        //}
        //#endregion

        #region Complete Handshake
        public Handshake CompleteHandshake(Handshake handshake)
        {           
            DateTime? saltExpiration = new DateTime();
            //using (var db = new WcfService1.ExcelEntities())
            using (var db = new WcfService1.RAMDevEntities())
            {
                try
                {
                    db.Connection.Open();
                    saltExpiration = db.GetSaltExpiration(handshake.sid).FirstOrDefault();
                }
                catch (Exception ex)
                {
                }
            }
            if (DateTime.Now < saltExpiration)
            {

                //string Credentials = Decrypt(handshake.Value1, handshake.Value2.Length, handshake.Value3, handshake.Value4);
                string Credentials = cryptor.Decrypt(handshake.Value1, handshake.Value2.Length, handshake.Value3, handshake.Value4);
                string[] UserAndPwd = Credentials.Split('|');
                int loginID = VerifyCredentials(UserAndPwd[0], UserAndPwd[1]);
                if (loginID > 0)
                {
                    handshake.Status = true;
                    handshake.lid = loginID;
                }
                else
                    handshake.Status = false;
            }
            else
            {
                handshake.Status = false;
            }
            return handshake;
        }
        #endregion

        #region GetSiteUnits
        public DataSet getSiteUnits(int dsid)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMDevSQL"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("SELECT * FROM GetSiteUnits(" + dsid + ")", conn);
                command.CommandType = CommandType.Text;
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsUnits = new DataSet();
                sda.Fill(dsUnits);
                return dsUnits;
            }
        }
        #endregion

        public string getProcessFamily(int dsid)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMDevSQL"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("select processfamily from Input.UnitGroupOverrides where datasetid =" + dsid, conn);
                command.CommandType = CommandType.Text;
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsUnits = new DataSet();
                sda.Fill(dsUnits);
                if (dsUnits.Tables.Count > 0)
                {
                    if (dsUnits.Tables[0].Rows.Count > 0)
                    {
                        return dsUnits.Tables[0].Rows[0]["processfamily"].ToString();
                    }
                }
                
                return "";
                
            }
        }
        public string getGenericProductName(int dsid)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMDevSQL"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("select GenericProductName from UnitInfo where datasetid =" + dsid, conn);
                command.CommandType = CommandType.Text;
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsUnits = new DataSet();
                sda.Fill(dsUnits);
                if (dsUnits.Tables.Count > 0)
                {
                    if (dsUnits.Tables[0].Rows.Count > 0)
                    {
                        return dsUnits.Tables[0].Rows[0]["GenericProductName"].ToString();
                    }
                }
                
                return "";
            }
        }
        public string getUnitInfoProcessType(int dsid)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMDevSQL"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("select strvalue from Answers where PropertyNo = 325 and datasetid =" + dsid, conn);
                command.CommandType = CommandType.Text;
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsUnits = new DataSet();
                sda.Fill(dsUnits);
                if (dsUnits.Tables.Count > 0)
                {
                    if (dsUnits.Tables[0].Rows.Count > 0)
                    {
                        return dsUnits.Tables[0].Rows[0]["strvalue"].ToString();
                    }
                }

                return "";
            }
        }
        public string getprimaryproductname(int dsid)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMDevSQL"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("select primaryproductname from UnitInfo where datasetid =" + dsid, conn);
                command.CommandType = CommandType.Text;
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsUnits = new DataSet();
                sda.Fill(dsUnits);
                if (dsUnits.Tables.Count > 0)
                {
                    if (dsUnits.Tables[0].Rows.Count > 0)
                    {
                        return dsUnits.Tables[0].Rows[0]["primaryproductname"].ToString();
                    }
                }

                return "";
            }
        }

        #region push Answers
        public byte[] pushAnswers(int dsid, byte[] salt, byte[] key, byte[] vector)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMDevSQL"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("GetQuestionAnswers", conn);
                //SqlCommand command = new SqlCommand("GetQuestionAnswersOnlyPrepop", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@DataSetID", dsid));
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsAnswers = new DataSet();
                sda.Fill(dsAnswers);
                dsAnswers.Tables[0].Columns.Add("UnitName", typeof(string));

                //new
                dsAnswers.Tables[0].Columns.Add("processfamily", typeof(string));
                dsAnswers.Tables[0].Columns.Add("GenericProductName", typeof(string));
                dsAnswers.Tables[0].Columns.Add("UnitInfoProcessType", typeof(string));
                dsAnswers.Tables[0].Columns.Add("primaryproductname", typeof(string));
                
                

                using (var db = new WcfService1.RAMDevEntities())
                {
                    try
                    {
                        dsAnswers.Tables[0].Rows[0]["UnitName"] = db.Dataset_LU.Single(c => c.DatasetID == dsid).FacilityName;

                        //new
                        dsAnswers.Tables[0].Rows[0]["processfamily"] = getProcessFamily(dsid);
                        dsAnswers.Tables[0].Rows[0]["GenericProductName"] = getGenericProductName(dsid);
                        dsAnswers.Tables[0].Rows[0]["UnitInfoProcessType"] = getUnitInfoProcessType(dsid);
                        dsAnswers.Tables[0].Rows[0]["primaryproductname"] = getprimaryproductname(dsid);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                StringWriter writer = new StringWriter();
                dsAnswers.WriteXml(writer, XmlWriteMode.WriteSchema);
                string xml = writer.ToString();
                
                return cryptor.Encrypt(xml, salt, key, vector);
            }
        }
        #endregion

        #region Compress Data
        public byte[] CompressData(string data)
        {
            var dataBytes = Encoding.UTF8.GetBytes(data);
            using (var memStreamIn = new MemoryStream(dataBytes))
            using (var memStreamOut = new MemoryStream())
            {
                using (var gzStream = new System.IO.Compression.GZipStream(memStreamOut, System.IO.Compression.CompressionMode.Compress))
                {
                    memStreamIn.CopyTo(gzStream);
                }
                return memStreamOut.ToArray();
            }
        }
        #endregion

        #region push Answers with history
        public byte[] pushAnswerswithhistory(int dsid, byte[] salt, byte[] key, byte[] vector)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMDevSQL"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("GetQuestionAnswersWithHistory", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@DataSetID", dsid));
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsAnswers = new DataSet();
                sda.Fill(dsAnswers);

                using (var db = new WcfService1.RAMDevEntities())
                {
                    try
                    {
                        dsAnswers.Tables[0].Rows.Add("UnitName", "String", System.DBNull.Value, System.DBNull.Value, db.Dataset_LU.Single(c => c.DatasetID == dsid).FacilityName);
                        dsAnswers.Tables[0].Rows.Add("processfamily", "String", System.DBNull.Value, System.DBNull.Value, getProcessFamily(dsid));
                        dsAnswers.Tables[0].Rows.Add("GenericProductName", "String", System.DBNull.Value, System.DBNull.Value, getGenericProductName(dsid));
                        dsAnswers.Tables[0].Rows.Add("UnitInfoProcessType", "String", System.DBNull.Value, System.DBNull.Value, getUnitInfoProcessType(dsid));
                        dsAnswers.Tables[0].Rows.Add("primaryproductname", "String", System.DBNull.Value, System.DBNull.Value, getprimaryproductname(dsid));
                    }
                    catch (Exception ex)
                    {
                    }
                }
                               
                StringWriter writer = new StringWriter();
                dsAnswers.WriteXml(writer, XmlWriteMode.WriteSchema);
                string xml = writer.ToString();

                return cryptor.Encrypt(CompressData(xml), salt, key, vector);
            }
        }
        #endregion

        #region push Answers Pre-Pop
        public byte[] pushAnswersPrePop(int dsid, byte[] salt, byte[] key, byte[] vector)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMDevSQL"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("GetQuestionAnswersOnlyPrepop", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@DataSetID", dsid));
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsAnswers = new DataSet();
                sda.Fill(dsAnswers);
                dsAnswers.Tables[0].Columns.Add("UnitName", typeof(string));
                using (var db = new WcfService1.RAMDevEntities())
                {
                    try
                    {
                        dsAnswers.Tables[0].Rows[0]["UnitName"] = db.Dataset_LU.Single(c => c.DatasetID == dsid).FacilityName;
                    }
                    catch (Exception ex)
                    {
                    }
                }
                StringWriter writer = new StringWriter();
                dsAnswers.WriteXml(writer, XmlWriteMode.WriteSchema);
                string xml = writer.ToString();

                return cryptor.Encrypt(xml, salt, key, vector);
            }
        }
        #endregion

        public string GetXMLData(byte[] xmlstringBytes, int loginID, byte[] salt, byte[] key, byte[] vector, double dsid)
        {
            string Result = string.Empty;
            try
            {
                //string xmlstring = Decrypt(xmlstringBytes, salt.Length, key, vector);
                string xmlstring = cryptor.Decrypt(xmlstringBytes, salt.Length, key, vector);
                MemoryStream memStream = new MemoryStream(Encoding.ASCII.GetBytes(xmlstring));
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                memStream.Position = 0;
                doc.Load(memStream);
                List<XmlElement> elList = new List<XmlElement>();
                System.Xml.XmlElement root = doc.DocumentElement;
                foreach (System.Xml.XmlElement el in root)
                {
                    elList.Add(el);
                }
                Result = SaveXMLValuesToDBNEW(elList, loginID, dsid);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            if (Result != "")
                return Result;
            else
                return "Data Received and Saved Successfully.";
        }

        private string SaveXMLValuesToDBNEW(List<XmlElement> ml, int? loginID, double dsid)
        {
            //using (var db = new WcfService1.ExcelEntities())
            using (var db = new WcfService1.RAMDevEntities())
            {
                try
                {
                    db.Connection.Open();

                    foreach (XmlElement el in ml)
                    {
                        db.spSaveAnswerWS(Convert.ToInt32(dsid), el.Name, null, null, el.InnerText, loginID);
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                return "";
            }
         }

        private string SaveXMLValuesToDB(WorksheetData wsd)
        {
            using (var db = new WcfService1.ExcelEntities())
            {
                try
                {
                    int? loginID = VerifyCredentials(wsd.Username.ToString(), wsd.Password.ToString());
                    if (loginID > 0)
                    {
                        db.Connection.Open();

                        db.InsertUnitData(wsd.UnitName.ToString(),
                                        loginID,
                                        wsd.TAAvgInterval.ToString(),
                                        wsd.TACausePcnt_FP.ToString(),
                                        wsd.TACausePcnt_IE.ToString(),
                                        wsd.TACausePcnt_RE.ToString(),
                                        wsd.TACausePcnt_Tot.ToString(),
                                        wsd.TACompressors.ToString(),
                                        wsd.TAContAvail.ToString(),
                                        wsd.TAControlValves.ToString(),
                                        wsd.TACptlProject.ToString(),
                                        wsd.TACraftmanMtCapWhrFP.ToString(),
                                        wsd.TACraftmanMtCapWhrIE.ToString(),
                                        wsd.TACraftmanMtCapWhrRE.ToString(),
                                        wsd.TACraftmanMtCapWhrTOT.ToString(),
                                        wsd.TACraftmanTotWhrFP.ToString(),
                                        wsd.TACraftmanTotWhrIE.ToString(),
                                        wsd.TACraftmanTotWhrRE.ToString(),
                                        wsd.TaskEmergency_RE.ToString(),
                                        wsd.TaskEmergency_Tot.ToString(),
                                        wsd.AnnTAMatlFP.ToString(),
                                        wsd.AnnTAMatlIE.ToString(),
                                        wsd.AnnTAMatlMtCapFP.ToString(),
                                        wsd.AnnTAMatlMtCapIE.ToString(),
                                        wsd.AnnTAMatlMtCapRE.ToString(),
                                        wsd.AnnTAMatlMtCapTOT.ToString(),
                                        wsd.AnnTAMatlRE.ToString(),
                                        wsd.AnnTAMatlTOT.ToString(),
                                        wsd.AnnTAMatlTotFP.ToString(),
                                        wsd.AnnTAMatlTotIE.ToString(),
                                        wsd.AnnTAMatlTotRE.ToString(),
                                        wsd.AnnTAMatlTotTOT.ToString(),
                                        wsd.EquipCntAgitatorsInVessels.ToString(),
                                        wsd.EquipCntAnalyzerBlending.ToString(),
                                        wsd.EquipCntAnalyzerEmissions.ToString(),
                                        wsd.EquipCntAnalyzerProcCtrl.ToString(),
                                        wsd.EquipCntBlowersFans.ToString(),
                                        wsd.EquipCntCentrifugesMain.ToString(),
                                        wsd.EquipCntCompressorsRecip.ToString(),
                                        wsd.EquipCntCompressorsRotating.ToString(),
                                        wsd.EquipCntControlValves.ToString(),
                                        wsd.EquipCntCoolingTowers.ToString(),
                                        wsd.EquipCntCrushers.ToString(),
                                        wsd.EquipCntDistTowers.ToString(),
                                        wsd.EquipCntDistVoltage.ToString(),
                                        wsd.EquipCntFurnaceBoilers.ToString(),
                                        wsd.EquipCntHeatExch.ToString(),
                                        wsd.EquipCntHeatExchFinFan.ToString(),
                                        wsd.EquipCntHeatExchOth.ToString(),
                                        wsd.EquipCntISBLSubStationTransformer.ToString(),
                                        wsd.EquipCntMotorsMain.ToString(),
                                        wsd.EquipCntPumpsCentrifugal.ToString(),
                                        wsd.EquipCntPumpsPosDisp.ToString(),
                                        wsd.EquipCntRefrigUnits.ToString(),
                                        wsd.EquipCntRotaryDryers.ToString(),
                                        wsd.EquipCntSafetyValves.ToString(),
                                        wsd.EquipCntSilosISBL.ToString(),
                                        wsd.EquipCntStorageTanksISBL.ToString(),
                                        wsd.EquipCntTurbines.ToString(),
                                        wsd.EquipCntVarSpeedDrives.ToString(),
                                        wsd.EquipCntVessels.ToString(),
                                        wsd.EventsAnalyzerBlending.ToString(),
                                        wsd.EventsAnalyzerEmissions.ToString(),
                                        wsd.EventsAnalyzerProcCtrl.ToString(),
                                        wsd.EventsCompressorsRecip.ToString(),
                                        wsd.EventsCompressorsRotating.ToString(),
                                        wsd.EventsControlValves.ToString(),
                                        wsd.EventsDistTowers.ToString(),
                                        wsd.EventsFurnaceBoilers.ToString(),
                                        wsd.EventsHeatExch.ToString(),
                                        wsd.EventsMotorsMain.ToString(),
                                        wsd.EventsPumpsCentrifugal.ToString(),
                                        wsd.EventsPumpsPosDisp.ToString(),
                                        wsd.EventsSafetyInstrSys.ToString(),
                                        wsd.EventsTurbines.ToString(),
                                        wsd.EventsVarSpeedDrives.ToString(),
                                        wsd.EventsVessels.ToString(),
                                        wsd.MTBFAnalyzerBlending.ToString(),
                                        wsd.MTBFAnalyzerEmissions.ToString(),
                                        wsd.MTBFAnalyzerProcCtrl.ToString(),
                                        wsd.MTBFCompressorsRecip.ToString(),
                                        wsd.MTBFCompressorsRotating.ToString(),
                                        wsd.MTBFControlValves.ToString(),
                                        wsd.MTBFDistTowers.ToString(),
                                        wsd.MTBFFurnaceBoilers.ToString(),
                                        wsd.MTBFHeatExch.ToString(),
                                        wsd.MTBFMotorsMain.ToString(),
                                        wsd.MTBFPumpsCentrifugal.ToString(),
                                        wsd.MTBFPumpsPosDisp.ToString(),
                                        wsd.MTBFSafetyInstrSys.ToString(),
                                        wsd.MTBFTurbines.ToString(),
                                        wsd.MTBFVarSpeedDrives.ToString(),
                                        wsd.MTBFVessels.ToString(),
                                        wsd.ProgramCBM_FP.ToString(),
                                        wsd.ProgramCBM_IE.ToString(),
                                        wsd.ProgramCBM_RE.ToString(),
                                        wsd.ProgramRCM_FP.ToString(),
                                        wsd.ProgramRCM_IE.ToString(),
                                        wsd.ProgramRCM_RE.ToString(),
                                        wsd.ProgramTimeBased_FP.ToString(),
                                        wsd.ProgramTimeBased_IE.ToString(),
                                        wsd.ProgramTimeBased_RE.ToString(),
                                        wsd.RelProcessImproveProg_FP.ToString(),
                                        wsd.RelProcessImproveProg_IE.ToString(),
                                        wsd.RelProcessImproveProg_RE.ToString(),
                                        wsd.RiskMgmtEquipRBI_FP.ToString(),
                                        wsd.RiskMgmtEquipRBI_IE.ToString(),
                                        wsd.RiskMgmtEquipRBI_RE.ToString(),
                                        wsd.RoutMatlFP.ToString(),
                                        wsd.RoutMatlIE.ToString(),
                                        wsd.RoutMatlMtCapFP.ToString(),
                                        wsd.RoutMatlMtCapIE.ToString(),
                                        wsd.RoutMatlMtCapRE.ToString(),
                                        wsd.RoutMatlMtCapTOT.ToString(),
                                        wsd.RoutMatlRE.ToString(),
                                        wsd.RoutMatlTOT.ToString(),
                                        wsd.RoutMatlTotFP.ToString(),
                                        wsd.RoutMatlTotIE.ToString(),
                                        wsd.RoutMatlTotRE.ToString(),
                                        wsd.RoutMatlTotTOT.ToString(),
                                        wsd.SeverityConstMaterials.ToString(),
                                        wsd.SeverityCorrosivity.ToString(),
                                        wsd.SeverityErosivity.ToString(),
                                        wsd.SeverityExplosivity.ToString(),
                                        wsd.SeverityFlammability.ToString(),
                                        wsd.SeverityFreezePt.ToString(),
                                        wsd.SeverityPressure.ToString(),
                                        wsd.SeverityProcessComplexity.ToString(),
                                        wsd.SeverityTemperature.ToString(),
                                        wsd.SeverityToxicity.ToString(),
                                        wsd.SeverityViscosity.ToString(),
                                        wsd.ShortOH2YrCnt.ToString(),
                                        wsd.ShortOHAnnHrsDown.ToString(),
                                        wsd.ShortOHAvgHrsDown.ToString(),
                                        wsd.ShortOHCausePcnt_FP.ToString(),
                                        wsd.ShortOHCausePcnt_IE.ToString(),
                                        wsd.ShortOHCausePcnt_RE.ToString(),
                                        wsd.ShortOHCausePcnt_Tot.ToString(),
                                        wsd.StoppagePcnt_FP.ToString(),
                                        wsd.StoppagePcnt_IE.ToString(),
                                        wsd.StoppagePcnt_RE.ToString(),
                                        wsd.StoppagePcnt_Tot.ToString(),
                                        wsd.TAAvgAnnHrsDown.ToString(),
                                        wsd.TAAvgHrsDown.ToString(),
                                        wsd.TAAvgHrsExecution.ToString(),
                                        wsd.TAAvgHrsShutdown.ToString(),
                                        wsd.TAAvgHrsStartup.ToString(),
                                        wsd.TACraftmanTotWhrTOT.ToString(),
                                        wsd.TACraftmanWhrFP.ToString(),
                                        wsd.TACraftmanWhrIE.ToString(),
                                        wsd.TACraftmanWhrRE.ToString(),
                                        wsd.TACraftmanWhrTOT.ToString(),
                                        wsd.TACritPath.ToString(),
                                        wsd.TACritPathOth.ToString(),
                                        wsd.TADeterioration.ToString(),
                                        wsd.TADiscretionaryInspect.ToString(),
                                        wsd.TADistTowersOpened.ToString(),
                                        wsd.TAFiredFurnaces.ToString(),
                                        wsd.TAFixedSchedule.ToString(),
                                        wsd.TAHeatExchOpened.ToString(),
                                        wsd.TALowDemand.ToString(),
                                        wsd.TAPumps.ToString(),
                                        wsd.TARegulatoryInspect.ToString(),
                                        wsd.TaskConditionMonitor_FP.ToString(),
                                        wsd.TaskConditionMonitor_IE.ToString(),
                                        wsd.TaskConditionMonitor_RE.ToString(),
                                        wsd.TaskConditionMonitor_Tot.ToString(),
                                        wsd.TaskPreventive_FP.ToString(),
                                        wsd.TaskPreventive_IE.ToString(),
                                        wsd.TaskPreventive_RE.ToString(),
                                        wsd.TaskPreventive_Tot.ToString(),
                                        wsd.TaskRoutCorrective_FP.ToString(),
                                        wsd.TaskRoutCorrective_IE.ToString(),
                                        wsd.TaskRoutCorrective_RE.ToString(),
                                        wsd.TaskRoutCorrective_Tot.ToString(),
                                        wsd.TaskTotal_FP.ToString(),
                                        wsd.TaskTotal_IE.ToString(),
                                        wsd.TaskTotal_RE.ToString(),
                                        wsd.TaskTotal_Tot.ToString(),
                                        wsd.TAUnplannedFailure.ToString(),
                                        wsd.TAValvesCalibrated.ToString(),
                                        wsd.TAValvesOpened.ToString(),
                                        wsd.TAWeather.ToString(),
                                        wsd.UnitInfoContinuosOrBatch.ToString(),
                                        wsd.UnitInfoMaxDailyProdRate.ToString(),
                                        wsd.TaskEmergency_FP.ToString(),
                                        wsd.TaskEmergency_IE.ToString(),
                                        wsd.UnitInfoPhasePredominentFeedstock.ToString(),
                                        wsd.UnitInfoPhasePredominentProduct.ToString(),
                                        wsd.UnitInfoPlantAge.ToString(),
                                        wsd.UnitInfoProdRateUOM.ToString(),
                                        wsd.UnitInfoPRV.ToString(),
                                        wsd.UnitInfoPRVUSD.ToString(),
                                        wsd.RoutCraftmanMtCapWhrFP.ToString(),
                                        wsd.RoutCraftmanMtCapWhrIE.ToString(),
                                        wsd.RoutCraftmanMtCapWhrRE.ToString(),
                                        wsd.RoutCraftmanMtCapWhrTOT.ToString(),
                                        wsd.RoutCraftmanTotWhrFP.ToString(),
                                        wsd.RoutCraftmanTotWhrIE.ToString(),
                                        wsd.RoutCraftmanTotWhrRE.ToString(),
                                        wsd.RoutCraftmanTotWhrTOT.ToString(),
                                        wsd.RoutCraftmanWhrFP.ToString(),
                                        wsd.RoutCraftmanWhrIE.ToString(),
                                        wsd.RoutCraftmanWhrRE.ToString(),
                                        wsd.RoutCraftmanWhrTOT.ToString(),
                                        wsd.LossHrsForAnnTA.ToString(),
                                        wsd.LossHrsForProcessReliability.ToString(),
                                        wsd.LossHrsForShortOH.ToString(),
                                        wsd.LossHrsTotalAnnProdLoss.ToString(),
                                        wsd.LossHrsForUnschedMaint.ToString());

                    }
                    else
                    {
                        return "Invalid Username/Password";
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            return "";
        }


        #region Old Stuff not being used

        public string GetData(string value)
        {
            //return string.Format("You entered: {0}", value);

            long count = 1000000000;
            for (int i = 0; i < count; i++)
            {
                string abc = string.Empty;
            }

            ExcelEntities excelEnt = new ExcelEntities();
            using (var db = new WcfService1.ExcelEntities())
            {
                try
                {
                    db.InsertData(value, "test", true);
                }
                catch (Exception ex)
                {
                    return "Error Receiving Data, Please Try Again.";
                }
            }
            return string.Format("Data Successfully Received. {0}", value);
        }

        #region Get Site Data
        public string GetSiteData(string xmlstring)
        {
            string Result = string.Empty;
            try
            {
                WorksheetData wsd = new WorksheetData();
                System.Xml.XmlReader xmlReader = new System.Xml.XmlTextReader(new System.IO.StringReader(xmlstring));
                System.Xml.Serialization.XmlSerializer xmlSer = new System.Xml.Serialization.XmlSerializer(typeof(WorksheetData));
                wsd = (WorksheetData)xmlSer.Deserialize(xmlReader);
                Result = SaveSiteData(wsd);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            if (Result != "")
                return Result;
            else
                return "Data Received and Saved Successfully.";
        }
        #endregion

        #region Save Site Data
        private string SaveSiteData(WorksheetData wsd)
        {
            using (var db = new WcfService1.ExcelEntities())
            {
                try
                {
                    int? loginID = VerifyCredentials(wsd.Username.ToString(), wsd.Password.ToString());
                    if (loginID > 0)
                    {
                        db.Connection.Open();
                        db.InsertSiteData(wsd.SiteName.ToString(), wsd.Country.ToString(), wsd.CompanyName.ToString(), loginID);
                    }
                    else
                    {
                        return "Invalid Username/Password";
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            return "";
        }
        #endregion

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
        #endregion

    }
}
