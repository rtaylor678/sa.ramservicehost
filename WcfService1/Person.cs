﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Xml;
using System.Data.SqlTypes;
using System.Xml.Serialization;
using System.Text;


    [XmlRoot("PersonInfoXml")]     
    public class Person
    {

        #region Constructor

        public Person()
        {            
           
        }
        public Person(Identifier identifier,Address address,Education education,List<string> skills)
        {
            mIdentify = identifier;
            mAddress = address;
            mEducation = education;
            mSkills = skills;
        }
        
    
        #endregion
      
        private Identifier mIdentify;
        private Address mAddress;
        private Education mEducation;
        
        [XmlElement("Name")]
        public Identifier PersonIdentify
        {
            get
            {
                if (mIdentify == null) mIdentify = new Identifier();
                return mIdentify; 
            }
            set { mIdentify = value; }
        }

        [XmlElement("Address")]
        public Address PersonAddress
        {
            get
            {
                if (mAddress == null)
                    mAddress = new Address();
                return mAddress; 
            }
            set { mAddress = value; }
        }

        [XmlElement("Education")]
        public Education PersonEducation
        {
            get
            {
                if (mEducation == null)
                    mEducation = new Education();
                return mEducation; 
            }
            set { mEducation = value; }
        }

        private List<string> mSkills;
        [System.Xml.Serialization.XmlArrayItemAttribute(ElementName = "Skill", IsNullable = false)]
        public List<string> Skills
        {
            get { return mSkills; }
            set { mSkills = value; }
        }

        public class Identifier
        {
            #region Construcor
            public Identifier()
            {

            }
            public Identifier(string firstname,string lastname,string sex)
            {
                mFirstName = firstname;
                mLastName = lastname;
                mSex = sex;
            }
            #endregion            

            #region Properties
            private String mFirstName;
            private String mLastName;
            private String mSex;          

            public String FirstName
            {
                get{return mFirstName;}
                set{mFirstName = value;}
            }
            public String LastName
            {
                get{return mLastName;}
                set{mLastName = value;}
            }
            public String Sex
            {
                get { return mSex; }
                set { mSex = value; }
            }            
            #endregion            
        }
        public class Address
        {
            #region Constructor  
           
            public Address()
            {
 
            }
            public Address(string address, string city, string state, string country,string phone)
            {
                mAddressS = address;
                mCity = city;
                mState = state;
                mCountry = country;
                mPhone = phone;
            }
            #endregion

            #region Properties
            private String mAddressS;
            private String mCountry;
            private String mCity;
            private String mState;
            private String mPhone;
            public String AddressS
            {
                get { return mAddressS; }
                set { mAddressS = value; }
            }   
            public String Country
            {
                get{return mCountry;}
                set{mCountry = value;}
            }
            public String City
            {
                get{return mCity;}
                set{mCity = value;}
            }
            public String State
            {
                get { return mState; }
                set { mState = value; }
            }
            public String Phone
            {
                get { return mPhone; }
                set { mPhone = value; }
            }
                  
            #endregion
        }
        public class Education
        {
            #region Constructor
            public Education()
            {

            }
            public Education(string bachelorDegree,string masterDegree)
            {
                mBachelor = bachelorDegree;
                mMaster = masterDegree;

            }
            #endregion
            #region Properties
            private String mBachelor;
            private String mMaster;
            
            public String Bachelor
            {
                get { return mBachelor; }
                set { mBachelor = value; }
            }
            public String Master
            {
                get { return mMaster; }
                set { mMaster = value; }
            }
          
            #endregion
        }
      
        /// <summary>
        /// Reconstruct an object from an XML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T DeserializeFromXml<T>(string xml)
        {
            T result;
            XmlSerializer ser = new XmlSerializer(typeof(T));
            using (TextReader tr = new StringReader(xml))
            {
                result = (T)ser.Deserialize(tr);
            }
            return result;
        }
        /// <summary>
        /// Serialize an object into an XML File
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static void SerializeToXml<T>(T obj, string fileName)
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            //Create a FileStream object connected to the target file   
            FileStream fileStream = new FileStream(fileName, FileMode.Create);           
            ser.Serialize(fileStream, obj);
            fileStream.Close(); 

            //SqlXml sqlXml;
            // MemoryStream stream = new MemoryStream();
            // using (XmlWriter writer = XmlWriter.Create(stream))
            //  {
            //  ser.Serialize(writer, obj);
            // sqlXml = new SqlXml(stream);
            // }
            // return sqlXml;
        }
        /// <summary>
        /// Serialize an object into an XML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeToXml<T>(T obj)
        {
            // To cut down on the size of the xml being sent to the database, we'll strip
            // out this extraneous xml.
            StringWriter Output = new StringWriter(new StringBuilder());
            XmlSerializer ser = new XmlSerializer(typeof(T));        
             
            ser.Serialize(Output, obj);
            return  Output.ToString();
              
        }
       
        //public static T DeserializeObject<T>(string xml)
        //{
        //    XmlSerializer xs = new XmlSerializer(typeof(T));
        //    MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xml));
        //    XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
        //    return (T)xs.Deserialize(memoryStream);
        //}

    }
   

