﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace WcfService1
{
    public class Crypt
    {
        #region Encrypt Text
        public byte[] Encrypt(string TextValue, Byte[] salt, byte[] key, byte[] vector)
        {
            RijndaelManaged rm = new RijndaelManaged();
            //Create an encryptor using our encryption method, key, and vector.
            ICryptoTransform EncryptorTransform = rm.CreateEncryptor(key, vector);

            //Used to translate bytes to text and vice versa
            UTF8Encoding UTFEncoder = new System.Text.UTF8Encoding();

            //Translates our text value into a byte array.
            Byte[] pepper = UTFEncoder.GetBytes(TextValue);

            int Salt = salt.Length;
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(salt);
            Byte[] bytes = new byte[2 * Salt + pepper.Length];
            System.Buffer.BlockCopy(salt, 0, bytes, 0, Salt);
            System.Buffer.BlockCopy(pepper, 0, bytes, Salt, pepper.Length);
            using (crypto)
            {
                crypto.GetNonZeroBytes(salt);
            }
            System.Buffer.BlockCopy(salt, 0, bytes, Salt + pepper.Length, Salt);

            //Used to stream the data in and out of the CryptoStream.
            MemoryStream memoryStream = new MemoryStream();

            CryptoStream cs = new CryptoStream(memoryStream, EncryptorTransform, CryptoStreamMode.Write);
            cs.Write(bytes, 0, bytes.Length);
            cs.FlushFinalBlock();

            memoryStream.Position = 0;
            byte[] encrypted = new byte[memoryStream.Length];
            memoryStream.Read(encrypted, 0, encrypted.Length);

            cs.Close();
            memoryStream.Close();
            rm.Dispose();

            return encrypted;
        }
        #endregion

        #region Encrypt Byte
        public byte[] Encrypt(byte[] TextValue, Byte[] salt, byte[] key, byte[] vector)
        {
            RijndaelManaged rm = new RijndaelManaged();
            //Create an encryptor using our encryption method, key, and vector.
            ICryptoTransform EncryptorTransform = rm.CreateEncryptor(key, vector);

            //Used to translate bytes to text and vice versa
            UTF8Encoding UTFEncoder = new System.Text.UTF8Encoding();

            //Translates our text value into a byte array.
            Byte[] pepper = TextValue;

            int Salt = salt.Length;
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(salt);
            Byte[] bytes = new byte[2 * Salt + pepper.Length];
            System.Buffer.BlockCopy(salt, 0, bytes, 0, Salt);
            System.Buffer.BlockCopy(pepper, 0, bytes, Salt, pepper.Length);
            using (crypto)
            {
                crypto.GetNonZeroBytes(salt);
            }
            System.Buffer.BlockCopy(salt, 0, bytes, Salt + pepper.Length, Salt);

            //Used to stream the data in and out of the CryptoStream.
            MemoryStream memoryStream = new MemoryStream();

            CryptoStream cs = new CryptoStream(memoryStream, EncryptorTransform, CryptoStreamMode.Write);
            cs.Write(bytes, 0, bytes.Length);
            cs.FlushFinalBlock();

            memoryStream.Position = 0;
            byte[] encrypted = new byte[memoryStream.Length];
            memoryStream.Read(encrypted, 0, encrypted.Length);

            cs.Close();
            memoryStream.Close();
            rm.Dispose();

            return encrypted;
        }
        #endregion

        #region Decrypt Byte
        public string Decrypt(byte[] EncryptedValue, int Salt, byte[] key, byte[] vector)
        {
            RijndaelManaged rm = new RijndaelManaged();
            //Create a decryptor using our encryption method, key, and vector.
            ICryptoTransform DecryptorTransform = rm.CreateDecryptor(key, vector);

            UTF8Encoding UTFEncoder = new System.Text.UTF8Encoding();

            MemoryStream encryptedStream = new MemoryStream();
            CryptoStream decryptStream = new CryptoStream(encryptedStream, DecryptorTransform, CryptoStreamMode.Write);
            decryptStream.Write(EncryptedValue, 0, EncryptedValue.Length);
            decryptStream.FlushFinalBlock();

            encryptedStream.Position = 0;
            Byte[] decryptedBytes = new Byte[encryptedStream.Length];
            encryptedStream.Read(decryptedBytes, 0, decryptedBytes.Length);
            encryptedStream.Close();

            // remove salt
            int len = decryptedBytes.Length - 2 * Salt;
            Byte[] pepper = new Byte[len];
            System.Buffer.BlockCopy(decryptedBytes, Salt, pepper, 0, len);
            return UTFEncoder.GetString(pepper);
        }
        #endregion
    }
}